Transform the data from TSV with TARQL (https://tarql.github.io/) to RDF and finally to N-Triples

    tarql hgg.sparql | rapper -i turtle -o ntriples - urn:base | sort -u > hgg.nt

Start the QuitStore (https://github.com/AKSW/QuitStore)

    quit -t .

You can access the store usually under http://localhost:5000/sparql

The data is from https://pcai042.informatik.uni-leipzig.de/swp/
